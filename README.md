# AO3 Formatter #

This is an automatic formatting program intended for converting raw text writing to an HTML-based format, specifically for [Archive of Our Own](http://www.archiveofoourown.org) (though it is not specifically bound to this website exclusively). The program is designed to allow for ease of adding additional, project-specific formatting rules that allow multi-chapter stories to have customisable formatting properties that are internal to their project. The focus is on simplicity of use and successful abstraction of code *in order to* make adding project-specific rules easy for the developer.

### Requirements ###

* Visual Studio 2012